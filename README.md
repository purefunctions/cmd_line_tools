# README #

This repo exists for the sole purpose of documenting a list of command line tools that I find useful

### Ansible based dotfiles and other setup ###
(original) - [https://github.com/elnappo/dotfiles]

### The list ###

* [Bat - cat(1) clone with syntax highlighting, git integration, etc.](https://github.com/sharkdp/bat)
